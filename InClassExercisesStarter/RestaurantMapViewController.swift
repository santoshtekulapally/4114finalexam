//
//  RestaurantMapViewController.swift
//  InClassExercisesStarter
//
//  Created by parrot on 2018-11-22.
//  Copyright © 2018 room1. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MapKit

class RestaurantMapViewController: UIViewController ,MKMapViewDelegate   {

    // MARK: Outlets
    @IBOutlet weak var mapView: MKMapView!
    
    var lat1:Double!
    var long1:Double!
    var nameone:String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("loaded the map screen")
        
        
        let URL = "https://opentable.herokuapp.com/api/restaurants?city=Toronto&per_page=5"
        
        Alamofire.request(URL, method: .get, parameters: nil).responseJSON {
            (response) in
            
            
            // -- put your code below this line
            
            if (response.result.isSuccess) {
                print("awesome, i got a response from the website!")
                print("Response from webiste: " )
                print(response.data)
                
                do {
                    let json = try JSON(data:response.data!)
                    
                    print(json)
                    
//                    // PARSING: grab the latitude and longitude
//                    print(json["latitude"])
//                    print(json["longitude"])
//
//                    print("------")
//
//                    // EXAMPLE:  Get the temperature
//
//                    //OPTIOAL: You could write the code like this:
//                    // let currently = json["currently"]
//                    // let temp = currently["temperature"]
//
//                    // or, you could write it like this (in one line)
                  //  let lat1 = json["restaurants"][0]["lng"]
                 //   let long1 = json["restaurants"][0]["lat"]
                  
                    let longone = json["restaurants"][0]["lng"]
                   let  latone = json["restaurants"][0]["lat"]
                    let  name = json["restaurants"][0]["name"]
        
                    
                    print("lat1 is  the value is  \(latone)")
                    print(longone)
                    
                    let x = CLLocationCoordinate2DMake(latone.double!,longone.double!)
                    
                    // pick a zoom level
                    let y = MKCoordinateSpanMake(0.01, 0.01)
                    
                    // set the region property of the mapview
                    let z = MKCoordinateRegionMake(x, y)
                    self.mapView.setRegion(z, animated: true)
//
                    
                    let pin = MKPointAnnotation()
                    
                    // 2. Set the latitude / longitude of the pin
                    pin.coordinate = x
                    
                    // 3. OPTIONAL: add a information popup (a "bubble")
                    pin.title = name.string!
                    
                    // 4. Show the pin on the map
                    self.mapView.addAnnotation(pin)
                    
           
                    
                    // annotation for restuarent 2 //
                  
                    let longtwo = json["restaurants"][1]["lng"]
                    let  lattwo = json["restaurants"][1]["lat"]
                    let  nametwo = json["restaurants"][1]["name"]
                    
                    print("lat1 is  the value is  \(latone)")
                    print(longone)
                    
                    let x1 = CLLocationCoordinate2DMake(lattwo.double!,longtwo.double!)
                    
                    // pick a zoom level
                    let y1 = MKCoordinateSpanMake(0.01, 0.01)
                    
                    // set the region property of the mapview
                    let z1 = MKCoordinateRegionMake(x1, y1)
                    self.mapView.setRegion(z1, animated: true)
                    //
                    
                    let pin1 = MKPointAnnotation()
                    
                    // 2. Set the latitude / longitude of the pin
                    pin1.coordinate = x1
                    
                    // 3. OPTIONAL: add a information popup (a "bubble")
                    pin1.title = nametwo.string!
                    
                    // 4. Show the pin on the map
                    self.mapView.addAnnotation(pin1)
                    
                    
                    /// annotation for resturanets 3
                    
                    
                    
                    let longthree = json["restaurants"][2]["lng"]
                    let  latthree = json["restaurants"][2]["lat"]
                    let  namethree = json["restaurants"][2]["name"]

                    
                    
                    print("lat1 is  the value is  \(latone)")
                    print(longone)
                    
                    let x2 = CLLocationCoordinate2DMake(latthree.double!,longthree.double!)
                    
                    // pick a zoom level
                    let y2 = MKCoordinateSpanMake(0.01, 0.01)
                    
                    // set the region property of the mapview
                    let z2 = MKCoordinateRegionMake(x2, y2)
                    self.mapView.setRegion(z1, animated: true)
                    //
                    
                    let pin2 = MKPointAnnotation()
                    
                    // 2. Set the latitude / longitude of the pin
                    pin2.coordinate = x2
                    
                    // 3. OPTIONAL: add a information popup (a "bubble")
                    pin2.title = namethree.string!
                    
                    // 4. Show the pin on the map
                    self.mapView.addAnnotation(pin2)
                    
                    /// annotation for resturanets 4
                    
                    let longfour = json["restaurants"][3]["lng"]
                    let  latfour = json["restaurants"][3]["lat"]
                    let  namefour = json["restaurants"][3]["name"]

                    
                    
                    print("lat1 is  the value is  \(latone)")
                    print(longone)
                    
                    let x3 = CLLocationCoordinate2DMake(latfour.double!,longfour.double!)
                    
                    // pick a zoom level
                    let y3 = MKCoordinateSpanMake(0.01, 0.01)
                    
                    // set the region property of the mapview
                    let z3 = MKCoordinateRegionMake(x3, y3)
                    self.mapView.setRegion(z3, animated: true)
                    //
                    
                    let pin3 = MKPointAnnotation()
                    
                    // 2. Set the latitude / longitude of the pin
                    pin3.coordinate = x3
                    
                    // 3. OPTIONAL: add a information popup (a "bubble")
                    pin3.title = namefour.string!
                    
                    // 4. Show the pin on the map
                    self.mapView.addAnnotation(pin3)
                    
// for the last resturent
                    
                    let longfive = json["restaurants"][4]["lng"]
                    let  latfive = json["restaurants"][4]["lat"]
                    let  namefive = json["restaurants"][4]["name"]

                    
                    
                    print("lat1 is  the value is  \(latone)")
                    print(longone)
                    
                    let x4 = CLLocationCoordinate2DMake(latfive.double!,longfive.double!)
                    
                    // pick a zoom level
                    let y4 = MKCoordinateSpanMake(0.01, 0.01)
                    
                    // set the region property of the mapview
                    let z4 = MKCoordinateRegionMake(x4, y4)
                    self.mapView.setRegion(z3, animated: true)
                    //
                    
                    let pin4 = MKPointAnnotation()
                    
                    // 2. Set the latitude / longitude of the pin
                    pin4.coordinate = x4
                    
                    // 3. OPTIONAL: add a information popup (a "bubble")
                    pin4.title = namefive.string!
                    
                    // 4. Show the pin on the map
                    self.mapView.addAnnotation(pin4)
                    
                    
                    
                }
                catch {
                    print ("Error while parsing JSON response")
                }
                
               
                
            }
            
        }
        
        
        
        
        // SWIFT: do somethign with the data
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    // MARK: Actions

    @IBAction func zoomInPressed(_ sender: Any) {
        
        print("zoom in!")
        var r = mapView.region
        
        print("Current zoom: \(r.span.latitudeDelta)")
        
        r.span.latitudeDelta = r.span.latitudeDelta / 4
        r.span.longitudeDelta = r.span.longitudeDelta / 4
        print("New zoom: \(r.span.latitudeDelta)")
        print("-=------")
        self.mapView.setRegion(r, animated: true)
        // HINT: Check MapExamples/ViewController.swift
    }
    
    @IBAction func zoomOutPressed(_ sender: Any) {
        // zoom out
        print("zoom out!")
        var r = mapView.region
        r.span.latitudeDelta = r.span.latitudeDelta * 2
        r.span.longitudeDelta = r.span.longitudeDelta * 2
        self.mapView.setRegion(r, animated: true)
        
        // HINT: Check MapExamples/ViewController.swift
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
