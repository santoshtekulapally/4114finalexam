//
//  MakeReservationViewController.swift
//  InClassExercisesStarter
//
//  Created by parrot on 2018-11-22.
//  Copyright © 2018 room1. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth

class MakeReservationViewController: UIViewController {

    // MARK: Outlets
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var dayTextField: UITextField!
    @IBOutlet weak var seatsTextField: UITextField!
    
    @IBOutlet weak var statuslbl: UILabel!
    // Mark: Firestore variables
    var db:Firestore!
    var name:String!
    
    // MARK: Default Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let currentUser = Auth.auth().currentUser;
        if (currentUser != nil) {
            print("We have a user!")
            print("User id: \(currentUser?.uid)")
            print("Email: \(currentUser?.email)")
            
            name = "\(currentUser?.email)"
        }
        else {
            print("no user is signed in.")
        }


        db = Firestore.firestore()
        
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: Actions
    @IBAction func buttonPressed(_ sender: Any) {
        
        
        
        var nameofres = nameTextField.text
        var days = dayTextField.text
        var seats = seatsTextField.text
        
        let resvRef = db.collection("Reservations")
        
        resvRef.document("resv").setData([
            "username" :name,
            "name of restaurant": nameofres,
            "day of the week": days,
            "Number of seats": seats
            ])

        statuslbl.text = "Reservations have been made successfully"
        
        print("pressed the button")
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
