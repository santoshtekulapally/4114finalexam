//
//  SeeReservationsViewController.swift
//  InClassExercisesStarter
//
//  Created by parrot on 2018-11-22.
//  Copyright © 2018 room1. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth

class SeeReservationsViewController: UIViewController {

    
    @IBOutlet weak var resultstxt: UITextField!
    @IBOutlet weak var namerest: UILabel!
    @IBOutlet weak var dateoftheweek: UILabel!
    @IBOutlet weak var nameuser: UILabel!
    @IBOutlet weak var seatno: UILabel!
    //MARK: Outlets
   
    
    
    // MARK: Firebase variables
    var db:Firestore!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        let currentUser = Auth.auth().currentUser;
        if (currentUser != nil) {
            print("We have a user!")
            print("User id: \(currentUser?.uid)")
            print("Email: \(currentUser?.email)")
            
        }
        else {
            print("no user is signed in.")
        }

        print("You are on the see reservations screen")
        
        db = Firestore.firestore()
        
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
        
        db.collection("Reservations").getDocuments() {
            
            (x, error) in
            
            
            
            if (x == nil){
                
                print("Error geting Document: \(error)")
                
                return
                
            }
            
            
            
            x?.documentChanges.forEach({
                
                (diff) in
                
                
                
                if (diff.type == DocumentChangeType.added) {
                    
                    
                    
                    let data = diff.document.data()
                                print("Error getting documents: \(diff.document.data())")

                    
                    let restaurant = data["name of restaurant"]
                    
                    let day = data["day of the week"]
                    
                    let numSeats = data["Number of seats"]
                    
                    var name:String! = (data["username"]!) as? String

                    
                    print(restaurant)
                    
                    print(day)
                    
                    print(numSeats)
                    
                    
                    
                    self.resultstxt.text = self.resultstxt.text! + "\(name) \(day): \(restaurant), \(numSeats) seats \n\n"
                    


                }
                
            })
        }
        
        
        
        
        
//        db.collection("Reservations").getDocuments() {
//            (querySnapshot, err) in
//
//            // MARK: FB - Boilerplate code to get data from Firestore
//            if let err = err {
//                print("Error getting documents: \(err)")
//            } else {
//                for document in querySnapshot!.documents {
//                    print("\(document.documentID) => \(document.data())")
//
//
//                    self.textField.text =  "\(document.data())"
//
//
//                        // 1. Get one result from database
//                        let data = document.data()
//
//
//
//                    self.namerest.text = (data["name of restaurant"]!) as? String
//                    self.dateoftheweek.text = (data["day of the week"]!) as? String
//                    self.nameuser.text = (data["username"]!) as? String
//                    self.seatno.text = (data["Number of seats"]!) as? String
//
        

                //}
           // }
      //  }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
